Selenium-cucumber-java-maven
selenium-cucumber : Automation Testing Using Java with Maven

Selenium-cucumber is a behavior driven development (BDD) approach to write automation test script to test Web

Pre-requisites
Java
Maven
Eclipse
Eclipse Plugins
Maven
Cucumber
Setting up selenium-cucumber-java
Install Java and set path.
Install Maven and set path.
Clone respective repository or download zip.
maven : https://github.com/selenium-cucumber/selenium-cucumber-java-maven-example

com.redshelf.pages--> POM page class for each feature file with related methods
com.redshelf.runner
com.redshelf.stepdefinitions
com.redshelf.utulities

QAProperties
RedshelfFeatures
