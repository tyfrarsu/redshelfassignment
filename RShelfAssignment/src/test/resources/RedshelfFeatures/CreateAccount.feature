Feature: SignUpCreateAccount

  Background: 
    Given User is on createaccountpage

  Scenario: SignUp with aslists
    When User creates new accounts with below credentials
      | ty               |
      | malcolm          |
      | tyarsu@gmail.com |
      | tyarsu@gmail.com |
      | NewPassword2!    |
      | NewPassword2!    |
    And User enters birthdate
    And User accepts termsofuse and privacynotice
    Then User should be able to create new account
