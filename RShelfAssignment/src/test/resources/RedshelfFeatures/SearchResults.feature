Feature: Student can add book to shoppingcard

  Background: User is on redshelf homepage

  Scenario: Student can search with single isbn
    When User search for the book with isbn "9780071836500"
    And User clicks on ViewDetails
    Then Isbn numbers should match

  Scenario Outline: Student can search with multiple isbns
    When User search for the "<isbns>"
    Then User should see matching book with same isbns

    Examples: 
      | isbns         |
      | 9780071836500 |
      | 0071836500 |
      | 9780071836500 |
