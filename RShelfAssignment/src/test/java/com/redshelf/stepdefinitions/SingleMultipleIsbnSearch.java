package com.redshelf.stepdefinitions;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import com.redshelf.pages.SearchPagePOM;
import com.redshelf.utulities.WebAppDriverSingletonPattern;
import com.redshelf.utulities.WebAppPrpReader;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SingleMultipleIsbnSearch {
	
	SearchPagePOM searchPagePOM = new SearchPagePOM();
	
	@When("User search for the book with isbn {string}")
	public void user_search_for_the_book_with_isbn(String stringFromFeature) {
	  
		WebAppDriverSingletonPattern.getDriver().get(WebAppPrpReader.getProperty("searchurl"));
		searchPagePOM.searchWithIsbn(stringFromFeature);
	}

	@When("User clicks on ViewDetails")
	public void user_clicks_on_view_details() {
	    
		searchPagePOM.clickViewDetails();
		
	}

	@Then("Isbn numbers should match")
	public void isbn_numbers_should_match() {
	 
		searchPagePOM.assertIsbns();
		WebAppDriverSingletonPattern.closeBrowserSession();
		
	}

	@When("User search for the {string}")
	public void user_search_for_the(String stringMultiple) {
		
		WebAppDriverSingletonPattern.getDriver().get(WebAppPrpReader.getProperty("searchurl"));
		searchPagePOM.searchWithIsbn(stringMultiple);
		searchPagePOM.clickViewDetails();
	    
	}

	@Then("User should see matching book with same isbns")
	public void user_should_see_matching_book_with_same_isbns() {
	   
		searchPagePOM.assertIsbns();
	}
}
