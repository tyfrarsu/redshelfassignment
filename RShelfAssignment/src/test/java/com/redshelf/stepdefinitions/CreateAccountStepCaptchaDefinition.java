package com.redshelf.stepdefinitions;

import java.util.List;


import com.redshelf.pages.CreateAnewAccountPOM;
import com.redshelf.utulities.WebAppDriverSingletonPattern;
import com.redshelf.utulities.WebAppPrpReader;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CreateAccountStepCaptchaDefinition {
	
	CreateAnewAccountPOM createAnewAccount = new CreateAnewAccountPOM();
	@Given("User is on createaccountpage")
	public void user_is_on_createaccountpage() {
		
		WebAppDriverSingletonPattern.getDriver().get(WebAppPrpReader.getProperty("createaccounturl"));
	    
	}

	@When("User creates new accounts with below credentials")
	public void user_creates_new_accounts_with_below_credentials(io.cucumber.datatable.DataTable dataTable) {
	
		
		List<String>	list	=	dataTable.asList(String.class);
		
		createAnewAccount.redShelfSignUp(list.get(0), list.get(1), list.get(2), list.get(3), list.get(4), list.get(5));
		
		
	}

	@When("User enters birthdate")
	public void user_enters_birthdate() {
		
	createAnewAccount.dateInputWithJavaScript();
	   
	}

	@When("User accepts termsofuse and privacynotice")
	public void user_accepts_termsofuse_and_privacynotice() {
	 
		createAnewAccount.checkBox();
		
	}

	@Then("User should be able to create new account")
	public void user_should_be_able_to_create_new_account() {
		
		createAnewAccount.clickToCreateAccount();
	    
	}

	

}
