package com.redshelf.stepdefinitions;

import java.util.concurrent.TimeUnit;

import com.redshelf.utulities.WebAppDriverSingletonPattern;

import io.cucumber.java.Before;

public class HookClassTestBase {

	@Before
	
	public void setUpRedShelfTest() {
		//ManageWindowWaits
		WebAppDriverSingletonPattern.getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebAppDriverSingletonPattern.getDriver().manage().window().maximize();
		
		
		
	}
	
}
