package com.redshelf.utulities;

import java.io.FileInputStream;
import java.util.Properties;

public class WebAppPrpReader {
	
	private static Properties pro;

	static {
		try {

			FileInputStream file = new FileInputStream(
					"src\\test\\resources\\QAProperties\\RedShelf.properties");

			pro = new Properties();

			pro.load(file);
			file.close();

		} catch (Exception e) {

			System.out.println("Property file path is not correct" + e.getMessage());

		}

	}

	public static String getProperty(String key) {

		return pro.getProperty(key);

		// Key will return value, same thing as parameters. browser will return "chrome"
	

	}

}
