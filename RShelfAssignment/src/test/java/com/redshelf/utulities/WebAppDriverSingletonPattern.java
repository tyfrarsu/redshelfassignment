package com.redshelf.utulities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


import io.github.bonigarcia.wdm.WebDriverManager;

public class WebAppDriverSingletonPattern {
	
	/*
	 * Singleton design pattern, POM (Page Object Model design pattern)
	 * 
	 */

	private static WebDriver driver = null;

	private WebAppDriverSingletonPattern() {

		

	}

	public static WebDriver getDriver() {

		if (driver == null) {

			switch (WebAppPrpReader.getProperty("browser")) {

			case "chrome":
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				break;

			case "firefox":

				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
				break;

			}

		}
		
		return driver;

	}
	
	public static void closeBrowserSession() {
		
		driver.close();
		driver.quit();
		driver= null;
		
		
		
	}
	
	
	
	

}
