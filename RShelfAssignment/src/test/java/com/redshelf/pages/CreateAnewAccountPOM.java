package com.redshelf.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.redshelf.utulities.WebAppDriverSingletonPattern;



public class CreateAnewAccountPOM {
	
public CreateAnewAccountPOM() {
		
		PageFactory.initElements(WebAppDriverSingletonPattern.getDriver(), this);
		
			
	}
	
	@FindBy (xpath ="//*[@id=\"first-name\"]")
	@CacheLookup
	
	private WebElement firstName;
	
	
	@FindBy(xpath= "//*[@id=\"last-name\"]")
	@CacheLookup
	private WebElement lastName;
	
	
	
	@FindBy(xpath= "//*[@id=\"email-address\"]")
	@CacheLookup
	private WebElement emailS;
	
	
	@FindBy(xpath= "//*[@id=\"email-address2\"]")
	@CacheLookup
	private WebElement confirmEmail;
	
	
	@FindBy(xpath= "//*[@id=\"password\"]")
	@CacheLookup
	private WebElement passWord;
	
	@FindBy(xpath= "//*[@id=\"password-confirm\"]")
	@CacheLookup
	private WebElement confirmPassWord;
	
	@FindBy(xpath= "//*[@id=\"birthdate\"]")
	@CacheLookup
	private WebElement dateCalender;
	
	
	@FindBy(xpath= "//*[@id=\"terms-of-use\"]")
	@CacheLookup
	private WebElement checkboxTerms;
	
	@FindBy(xpath= "//*[@id=\"privacy-policy\"]")
	@CacheLookup
	private WebElement checkboxPrivacy;

	@FindBy(xpath= "//*[@id=\"register-button\"]")
	@CacheLookup
	private WebElement createAccountButton;
	
	public void redShelfSignUp(String fName, String lName, String email, String cEmail, String pWord, String cPassWord ) {
		
		firstName.sendKeys(fName);
		lastName.sendKeys(lName);
		emailS.sendKeys(email);
		confirmEmail.sendKeys(cEmail);
		passWord.sendKeys(pWord);
		confirmPassWord.sendKeys(cPassWord);
		
	}
	
	public  void dateInputWithJavaScript() {
		 JavascriptExecutor js = (JavascriptExecutor)WebAppDriverSingletonPattern.getDriver();
		 js.executeScript("arguments[0].value='11/11/1981';",dateCalender );
	
	}
	
	public void checkBox(){
		
		checkboxTerms.click();
		checkboxPrivacy.click();
		
		
	}
	
	public void clickToCreateAccount() {
		
		createAccountButton.click();
	}
	

}
