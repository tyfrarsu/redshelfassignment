package com.redshelf.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.redshelf.utulities.WebAppDriverSingletonPattern;

public class SearchPagePOM {
	
public SearchPagePOM() {
		
		PageFactory.initElements(WebAppDriverSingletonPattern.getDriver(), this);
		
			
	}
	

	@FindBy (xpath ="//*[@id=\"search-catalog-navbar\"]")
	@CacheLookup
	
	private WebElement searchBar;

	
	@FindBy (xpath ="//*[@id=\"burger-nav\"]/div/div[1]/form/button/span")
	@CacheLookup
	
	private WebElement searchButton;
	
	@FindBy (xpath ="//*[@id=\"search-book-details\"]/a/div")
	@CacheLookup
	
	private WebElement viewDetails;
	
	@FindBy (xpath ="//*[@id=\"additional-book-details\"]/div/div/div[3]/table/tbody/tr[2]/td")
	@CacheLookup
	
	private WebElement isbnNumbersLocator;
	
	public void searchWithIsbn(String isbnNumber) {
		
		searchBar.sendKeys(isbnNumber +Keys.ENTER);
		//searchButton.click();
		
		
	}
	
	public void clickViewDetails() {
		
		viewDetails.click();
	}
	
	public void assertIsbns() {
		
		String expected = "9780071836500";
		org.junit.Assert.assertNotEquals(expected, isbnNumbersLocator.getText().contains(expected));
	}
	
	
}
